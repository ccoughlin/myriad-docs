# Myriad-Docs
This is the source for the Myriad documentation.  Included in the project are the Markdown files for generating documentation for:

* Myriad Framework
* Myriad Trainer, a tool for training and testing ROI detection models based on machine learning techniques
* Myriad Desktop, an easy-to-use tool for creating concurrent "parallel pipelines" for searching for ROI within large datasets.

## Requirements
* [MkDocs](http://www.mkdocs.org/)

## Usage
To generate the static HTML documentation for Myriad, run the following command:

````mkdocs build````

