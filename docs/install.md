# Installation

## System Requirements
Myriad will run anywhere where Java 8 is available.  The current minimum requirements for Java 8 on the desktop are summarized below.

* Memory: 128MB RAM
* Disk: 126MB
* Processor: Pentium 2 266MHz or better

Full details on Java 8 requirements are available from [Oracle's Java site](http://www.oracle.com/technetwork/java/javase/certconfig-2095354.html) .  Myriad has been tested to compile and run on Windows 7, Windows 10, various Linux distributions, TrueOS, OpenBSD, and FreeBSD.

## Dependencies
* JDK 8, either [Oracle Java](http://java.sun.com) or [OpenJDK](http://openjdk.java.net/) .  The Myriad library, Myriad Trainer and Myriad Desktop have not undergone extensive testing under OpenJDK but are known to compile and run.
* [Apache Maven](http://maven.apache.org)
* (Optional) [Git](http://git-scm.com) 

Maven requires the environment variable `JAVA_HOME` to be set.  Under Windows, press the Windows key to begin searching and start typing "environment" to bring up the System Properties dialog.  Click the `Environment Variables...` button and add a new User Variable `JAVA_HOME` that points to the root folder where the JDK was installed e.g. `C:\Program Files\Java\jdk1.8.0_101`.

POSIX-style operating systems such as Linux or OpenBSD will vary on how to set environment variables.  In the bash shell for example this can be done by adding the environment variable to the user’s `.bashrc file`, e.g. `export JAVA_HOME=/home/username/jdk`.  In csh the equivalent is `setenv JAVA_HOME /home/username/jdk` .

For convenience, you may also want to add both the JDK bin and the Maven bin folders to the path.

If you would like to track updates to the Myriad codebase, Git is recommended.  There are three primary Git repositories:

* Myriad [https://gitlab.com/ccoughlin/datareader.git](https://gitlab.com/ccoughlin/datareader.git) 
* Myriad Desktop [https://gitlab.com/ccoughlin/MyriadDesktop.git](https://gitlab.com/ccoughlin/MyriadDesktop.git) 
* Myriad Trainer [https://gitlab.com/ccoughlin/MyriadTrainer.git](https://gitlab.com/ccoughlin/MyriadTrainer.git) 

Alternatively, you can simply unpack the source code archives to a convenient location.

### OpenCL
Myriad has experimental support for [OpenCL](https://www.khronos.org/opencl/).  To use this optional feature, you will need to download and install OpenCL support for your system depending on which devices you wish to use:

* AMD CPU / GPU [http://developer.amd.com/tools-and-sdks/opencl-zone/](http://developer.amd.com/tools-and-sdks/opencl-zone/) 
* NVIDIA GPU [https://developer.nvidia.com/opencl](https://developer.nvidia.com/opencl) 
* Intel CPU / GPU and Xeon Phi accelerator [https://software.intel.com/en-us/intel-opencl](https://software.intel.com/en-us/intel-opencl)  

Myriad relies on an external dynamic library to access hardware.  The Myriad source code ships with pre-built libraries in the core\src\main\resources folder for 32-bit and 64-bit Windows and Linux operating environments.  The library appropriate to your system must be accessible at run-time, e.g. on Windows the library should be on the `PATH` (or copied to `C:\WINDOWS\System32` ) and on Linux should be on `LD_LIBRARY_PATH`.  Alternatively, if running a Myriad application from the command line, the OpenCL library path can be provided using a compiler directive such as

`java -jar <appname>.jar -Djava.library.path=/path/to/opencl_dlls`

For assistance using OpenCL on other operating systems, please [contact Emphysic](https://emphysic.com/contact-us/).  OpenCL is an optional component of Myriad; applications written using Myriad (including the GUI tools) will run perfectly well without OpenCL support.

## Building Myriad
### Automatic
Myriad is distributed as an [Apache Maven](https://maven.apache.org/) project.  Open a command prompt in the project's root folder and run `install.bat` (Windows) or `install.sh` (Linux/FreeBSD/OS X) to automatically download the required dependencies and install Myriad.

After Myriad has been installed, Desktop and Trainer can be installed by opening a command prompt in their respective root folders and again running `install.bat` (Windows) or `install.sh` (Linux/FreeBSD/OS X).

### Manual
If you'd prefer to do things manually and are comfortable with using Maven on the command line, read on.

Most of the Myriad dependencies will be automatically downloaded and installed, but several third-party libraries require an extra step.  These libraries are in the `libs` folder, which can be installed by issuing the following commands:

```
mvn install:install-file -Dfile=sgtdk-0.1.1.jar -DgroupId=com.github -DartifactId=sgdtk -Dversion=1.0.0 -Dpackaging=jar

mvn install:install-file -Dfile=pixelmed.jar -DgroupId=com.pixelmed -DartifactId=dicomtoolkit -Dversion=1.0.0 -Dpackaging=jar

mvn install:install-file -Dfile=aparapi.jar -DgroupId=com.amd -DartifactId=aparapi  -Dversion=1.0.0 -Dpackaging=jar
```

With the third-party libraries installed, switch back to the Myriad library root folder (i.e., the folder with the pom.xml file) and run the command `mvn clean install -DskipTests=true`.  Maven will download all the Myriad dependencies and if all goes well a minute or two later the main Myriad library will be built and installed to the local maven repository e.g. on Windows `C:\Users\<username>\.m2\repository`.

Now switch to the Myriad Desktop and Myriad Trainer project folders and run `mvn package` from each to build the GUIs.  Both GUIs depend on the Myriad library and will not compile until the library has been successfully installed.

### Expected Results
If compilation of the library and the GUIs was successful you should find a new Emphysic folder under the Maven repository at e.g. `.m2\repository\com\emphysic` .  Each of the source code folders should also contain one or more JAR files under the target sub-folder.  A JAR is a ZIP file with a [specific layout](https://docs.oracle.com/javase/tutorial/deployment/jar/) used in Java to bundle a compiled application.  Depending on your system configuration, you may be able to run the GUI tools simply by double-clicking the JAR file in the file manager.  If not, you can run the JAR by opening a command console and issuing a command:

* Myriad Desktop `java -jar <path>/desktop-1.0-SNAPSHOT-allinone.jar`
* Myriad Trainer `java -jar <path>/MyriadTrainer-1.0-SNAPSHOT.jar`

Although building the Myriad library does produce a JAR, there is no executable in this archive.  In this case the JAR is built as a dependency for other applications such as the GUI tools, analogous to a DLL or .so file in Windows or Linux respectively.  Applications you write that use the Myriad library will add this JAR to the project.
