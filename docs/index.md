# Myriad ROI Detection Toolkit

[Myriad](https://www.emphysic.com/myriad/) is a fault-tolerant distributed computing framework for creating [Region Of Interest (ROI)](https://en.wikipedia.org/wiki/Region_of_interest) detection applications.  Developed by [Emphysic](https://emphysic.com/), Myriad provides signal / image processing, machine learning, and concurrency functionality all in a single library.

In 2016 [NASA funded](http://sbir.nasa.gov/SBIR/abstracts/16/sbir/phase1/SBIR-16-1-H13.01-8360.html) the development of a Myriad-based system for automatically detecting indications of structural damage in large [Nondestructive Testing (NDT)](https://en.wikipedia.org/wiki/Nondestructive_testing) datasets.  

![Sample ROI Detection Results](img/sample_results.png)

We've put together a demonstration of this system that goes all the way from picking training data to searching for damage: 

[![Training / Testing Models](img/bigvid.png)](https://emphysic.wistia.com/medias/sbbft90a67)

## Potential Applications
* [Face detection](https://en.wikipedia.org/wiki/Face_detection)
* [Pedestrian tracking](http://www.bmva.org/apps:pedestrian)
* Object identification
* Obstacle avoidance

## Features
* A variety of signal/image processing operations and feature detection operations
* Machine learning capabilities courtesy [Smile](http://haifengl.github.io/smile/) and [Apache Mahout](http://mahout.apache.org/)
* Fault-tolerant concurrency courtesy [Akka](http://akka.io/)
* Experimental support for hardware acceleration (AMD Radeon, NVIDIA GeForce, etc.) courtesy [Aparapi](http://aparapi.github.io/)

## Licensing
Licensed under the [Apache 2.0 License](http://www.apache.org/licenses/LICENSE-2.0.txt)

## Additional Resources
* [Myriad Home Page](https://emphysic.com/myriad/)
* [Frequently Asked Questions](https://emphysic.com/myriad/faq/)
* [Sample Code and Videos](https://emphysic.com/myriad/downloads/)

## Contact Us
Questions? Comments? Suggestions?  [Drop us a line](https://emphysic.com/contact-us/).