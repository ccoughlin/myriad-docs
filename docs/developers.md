# Developers

## Git Repositories
* Myriad [https://gitlab.com/ccoughlin/datareader.git](https://gitlab.com/ccoughlin/datareader.git) 
* Myriad Desktop [https://gitlab.com/ccoughlin/MyriadDesktop.git](https://gitlab.com/ccoughlin/MyriadDesktop.git) 
* Myriad Trainer [https://gitlab.com/ccoughlin/MyriadTrainer.git](https://gitlab.com/ccoughlin/MyriadTrainer.git) 

## Developer Documentation
* [Java API documents](http://myrdocs.azurewebsites.net/api/)
* [Code Samples](https://emphysic.com/myriad/sample-code/)
* [Sample ROI Project Code](https://gitlab.com/ccoughlin/myriad-demo)